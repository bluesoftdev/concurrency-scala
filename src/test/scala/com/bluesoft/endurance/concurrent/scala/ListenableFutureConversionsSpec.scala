/**
 * Copyright 2015 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.concurrent.scala

import com.bluesoft.endurance.concurrent.ListenableFuture
import com.bluesoft.endurance.concurrent.SettableListenableFuture

import java.util.concurrent.ExecutionException
import org.scalatest.Matchers
import org.scalatest.testng.TestNGSuite
import org.scalatest.concurrent.{ AsyncAssertions, ScalaFutures }
import org.slf4j.LoggerFactory
import org.testng.annotations.Test
import scala.concurrent.{ ExecutionContext, Future }
import scala.util.{ Failure, Success }

class ListenableFutureConversionsSpec extends TestNGSuite with Matchers with ScalaFutures with AsyncAssertions {
  def LOG = LoggerFactory.getLogger(classOf[ListenableFutureConversionsSpec])
  import ListenableFutureConversions._
  import ExecutionContext.Implicits.global

  @Test def testConvertToListenableFuture() {
    val scalaFuture = Future {
      Thread.sleep(100L)
      "The quick brown fox jumped over the lazy dogs."
    }

    val listenableFuture: ListenableFuture[String] = scalaFuture

    listenableFuture.addListener(new Runnable() {
      def run() {
        try {
          val str = listenableFuture.getUninterruptibly
          LOG.info(s"got success with: $str")
        } catch {
          case ex: ExecutionException =>
            LOG.error(s"got error!", ex)

        }
      }
    })

    val str = listenableFuture.getUninterruptibly
    str should be("The quick brown fox jumped over the lazy dogs.")
  }

  @Test def testConvertToListenableFutureFailure() {
    val scalaFuture = Future {
      Thread.sleep(100L)
      throw new IllegalStateException("test exception")
    }

    val listenableFuture: ListenableFuture[String] = scalaFuture

    listenableFuture.addListener(new Runnable() {
      def run() {
        try {
          val str = listenableFuture.getUninterruptibly
          LOG.info(s"got success with: $str")
        } catch {
          case ex: ExecutionException =>
            LOG.error(s"got error!", ex)

        }
      }
    })

    val ex = the [ExecutionException] thrownBy listenableFuture.getUninterruptibly
    ex.getCause shouldBe a [IllegalStateException]
    ex.getCause.getMessage should be ("test exception")
  }
  
  @Test def testConversion() {
    val settableF = new SettableListenableFuture[String]()

    val scalaF: Future[String] = settableF

    scalaF.andThen({
      case Success(str) =>
        LOG.info(s"got success with: $str")
      case Failure(ex) =>
        LOG.error(s"got error!", ex)
    })

    settableF.set("The quick brown fox jumped over the lazy dogs.")
    whenReady(scalaF) { str =>
      str should be("The quick brown fox jumped over the lazy dogs.")
    }
  }

  @Test def testErrorPropagation() {
    val settableF = new SettableListenableFuture[String]()

    val scalaF: Future[String] = settableF

    scalaF.andThen({
      case Success(str) =>
        LOG.info(s"got success with: $str")
      case Failure(ex) =>
        LOG.error(s"got error!", ex)
    })

    settableF.setException(new IllegalStateException("test exception."))
    val w = new Waiter
    scalaF onComplete {
      case Failure(e) =>
        w(throw e); w.dismiss()
      case Success(_) => w.dismiss()
    }
    intercept[IllegalStateException] {
      w.await
    }
  }
}
