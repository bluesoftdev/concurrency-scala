# Endurance Concurrency - Scala #

Scala adapters for the [Endurance Concurrency](http://bitbucket.org/bluesoftdev/concurrency) classes.

## Usage ##

To use in your project, just depend on the latest version.  There is no released version yet, so the current version is 1.0.0-SNAPSHOT.

Gradle:

```
#!groovy

dependencies {
  ...
  compile 'com.bluesoftdev.endurance:concurrency-scala_2.11:1.0.0-SNAPSHOT'
}
```

Maven:
```
#!xml

<dependency>
  <groupId>com.bluesoftdev.endurance</groupId>
  <artifactId>concurrency-scala_2.11</artifactId>
  <version>1.0.0-SNAPSHOT</version>
</dependency>
```

SBT:
```
#!scala
libraryDependencies ++= "com.bluesoftdev.endurance" %% "concurrency-scala" % "1.0.0-SNAPSHOT"
```

## How to Use ##

Right now all that is provided is an implicit conversion from a ListenableFuture in Endurance Concurrency to a Scala Future.  To get the conversion simply import the class com.bluesoft.endurance.concurrency.scala.ListenableFutureConversions, like so:

```
#!scala
import com.bluesoft.endurance.concurrency.scala.ListenableFutureConversions._
```

## Contributions ##

Pull requests are encouraged.  Please fork and make your changes.  We use the [Git Flow](http://nvie.com/posts/a-successful-git-branching-model/) to manage the source code life cycle, so please make any changes to 'develop'.